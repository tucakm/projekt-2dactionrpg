﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Kontenjer o Entity objektima
//Sve instance su public, pošto objekt služi kao kontenjer podataka, a unity omogućuje pregled public varijabli unutar Inspectora 
//Stoga postavljanje na public olakšava izgradnju projekta 

public abstract class Entity {
    public int id; //identifikacijski broj 
    public string name; //ime
    public int level; //razina iskustva
    public int maxHealth; // maksimalno zdravlje
    public int currentHealth; // trenutno zdravlje 
    public int defense; // obrana
    public int physicalDamage; // napad
    public Vector2 Position; // pozicija
    public bool dead = false; // aktivnost objekta

}
