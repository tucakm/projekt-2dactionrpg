﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NPC : Entity {
 
    public bool holdingQuest; // označava da li npc sadrži quest
    public int totalQuests; //ukupan broj questova što npc sadrži 
    public int questsToCompliete; // broj questova preostalih za završiti

}
