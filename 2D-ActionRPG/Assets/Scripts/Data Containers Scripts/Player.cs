﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Player : Entity
{    
    public int gold; //novac
    public int exp; //iskustvo
    public int dashIndicator; // prikaz ukupnih dash mogučnosti
    public int[] levelsExpNeeded; // red sa definiranim vrijednostima iskustva potrebnim za iduću razinu  


    //Provjera da li igrač može povećati razinu iskustva
    public void CheckIfLevelUp(int expGiven){
        exp += expGiven;
        for (int i= level - 1; i < levelsExpNeeded.Length ; i++)
        {
            if (exp >= levelsExpNeeded[i])
            {
                level++;
                maxHealth += maxHealth / 2;
                defense += defense / 4;
                physicalDamage += physicalDamage / 3;
                currentHealth = maxHealth; 
            }
        }      
    }
   
}
