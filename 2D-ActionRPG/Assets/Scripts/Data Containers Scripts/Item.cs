﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*Klasa za prikaz itema unutar scene,
 * TO-DO kreirati podklase, za popliže označavanje itema
 * **/
[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class Item: ScriptableObject {

    public int item_id; // id 
    public string itemName = "New Item"; 
    public Sprite icon ; // referenca na sličicu
    public Vector2 postion;
  
    public int active = 1;  //prikaz aktivnosti unutar scene 1(Aktivno) 0 (NeaktivnO)
    public int used = 0; // flag da li je item iskorišten 0(neiskorišten) 1 (iskorišten)
    public string type; // tip itema
    public int effect; // effekt što daje igraču
    [TextArea]
    public string description; // opis 
    private void Awake()
    {
        type = "Healing";
        effect = 50;

        icon = Resources.Load<Sprite>("_Items/_Potions/pt1"); // iz mape Resources učitava se sličica Potiona
    }
    //inicijalizacija osnovih vrijednosti
    public void Init(string name,int active,int used)
    {
        this.name = name;
        this.active = active;
        this.used = used;
    }

    //korištenje
    public bool Use()
    {

        if (type == "Healing")
        {
            //provjera stanja zdravlja playera
            if (PlayerController.playerInstance.player.currentHealth == PlayerController.playerInstance.player.maxHealth)
            {
                //nedopustiti izvođenje ostatka 
                return false;
            }
            else
            {
                //puni trenutnog zdravlja za vrijednost effecta
                PlayerController.playerInstance.player.currentHealth += effect;
                //u slučaju da vrijednost trenutnog zdravlja prelazi vrijednost maksimalnog 
                if (PlayerController.playerInstance.player.currentHealth > PlayerController.playerInstance.player.maxHealth)
                {   
                    //postaviti na vrijednost trenutnog zdravlja
                    PlayerController.playerInstance.player.currentHealth = PlayerController.playerInstance.player.maxHealth;
                }

                this.used = 1;
                //pošto je iskorišten item, brisanje iz inventorija
                InventoryController.instance.Delite(this);
                return true;
            }

        }
        else {
            return false;
        }



       
    }
}
