﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Enemy : Entity
{
    public string enemyType; // tip neprijatelja
    public int giveExp; // količina iskustva što neprijatelj daje
    public int deadInd=0; //varijabla inicijalizaciju podatka iz baze

    // Metoda za reguliranje koliko iskustva daje neprijatelj    
    public int ExpGiving(int playerLevel) {
        int expGiver=0;
        if ((level - playerLevel) == 0)
        {
            expGiver = giveExp;
        }
        else if ((level - playerLevel) > 0)
        {
            expGiver = giveExp * level;
        }
        else {
            expGiver = giveExp / level;
        }
        return expGiver;
    }

}
