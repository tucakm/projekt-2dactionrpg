﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 *Uništava objekt kroz određeno vrijeme( čisti ga iz memorije) 
 */
public class DestroyOverTime : MonoBehaviour {

    [SerializeField]
    private float timeToDestory;
	void Start () {
		
	}
	
	void Update () {
        timeToDestory -= Time.deltaTime;

        if (timeToDestory <= 0) {
            Destroy(gameObject);
        }

    }
}
