﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*Klasa za kontroli itema unutar scene
 * */
public class ItemsController : MonoBehaviour {

    public Item item; //referenca na item
    private float _radius = 3f; 
    private CircleCollider2D pickupZone; //colider za aktivaciju mogućnosti spremanaj itema

    [SerializeField]
    private GameObject pressFtoPickUp; // referenca na tekst press f to pick up

    private void Start() {
        pickupZone = GetComponent<CircleCollider2D>(); //dohvaćanje circle colidera
        pickupZone.radius = _radius; 
        pressFtoPickUp.SetActive(false);
      }
    //u slučaju ulaženja unutar circle colidera
    private void OnTriggerStay2D(Collider2D collision)
    {
        pressFtoPickUp.SetActive(true);
        if (Input.GetKeyUp(KeyCode.F))
        {
          
            PickUp();

        }
    }
    // prilikom izlaska iz colidera
    private void OnTriggerExit2D(Collider2D collision)
    {        
        pressFtoPickUp.SetActive(false);
    }
    //spremanje itema unutar inventorya
    public void PickUp() {
        item.itemName = this.gameObject.name;
        //dodavanje unutar liste itema unutar InventoryController klase
        bool wasPickedUP = InventoryController.instance.Add(item);      
        if (wasPickedUP)
        {
            item.active = 0;
            //Dodavanje unutar liste pokupljenih itema
            SceenController.itemsPickedUP.Add(this.gameObject.name);
            //inaktivacija objekta i f to pick up teksta
            pressFtoPickUp.SetActive(false);
            this.gameObject.SetActive(false);
        }   
    }
    
    public void ResetItems() {
        item.active = 1;
        item.used = 0;
    }
}
