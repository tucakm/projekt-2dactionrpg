﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
/**
 * Klasa odogovorna za kontrolu lika igrača i registraciju ulaznih parametara (tipke na tipkovnici) 
 * */
public class PlayerController : EntityController
{
    /**
     * Static instanca dostupna svim klasama
     * Provjera da li instanca prilikom kreacije objekta već postoji
     * u slućaju da postoji ne dopustiti kreiranje nove
     */
    public static PlayerController playerInstance;
    private void Awake()
    {
        if (playerInstance != null)
        {
            return;
        }
        playerInstance = this;
        if (SceenController.sceeneInstance.player.player != null)
        {
            this.player = SceenController.sceeneInstance.player.player;
        }   
               
    }
    /**
     *[SerializeField] dopušta da privatne varijable budu dostupne unutar Unity Editor/Inspector
     * 
     * UserInterface Elementi
     */
    [SerializeField]
    private StatsController healthUI; //pokazatelj "života" na ekranu, 
    [SerializeField]
    private StatsController dashUI; //pokazatelj "dash" vještine na ekranu /plavi pravokutnik gornji ljevi kut
    [SerializeField]
    private Text levelText; //pokazatelj razine napredka na ekranu 
    [SerializeField]
    private Text characterName; //pokazatelj imena lika 
    [SerializeField]
    private GameObject gameOver; //kraj igre ekran 

    [SerializeField]
    private float dashSpeed; //brzina izvođenja vještnine "Dash"
    [SerializeField]
    private float dashTime; //vrijeme koliko vještina "Dash" traje 
   
    [SerializeField]
    private float dashColldownTime; // vrijeme čekanja da se vrati mogućnost dasha
    private float _keepHodlerOfColldown; // pamćenje vrijednosti dashColldownTime

    [SerializeField]
    private int numberDashes; //trenutna vrijednost preostalih mogucnosti da iskoristimo "Dash" maksimalna vrijednost 5
    private Coroutine _dashRoutine; //dash Corutina

    public Player player; //skladište podataka o igraču
    public bool isTalking; //trigger da li igrač ima dialog s Npc-om 

    public GameObject saveSucc; //referenca na saveSuccesfull panel
    
    //Inicijalizacija osnovnih varijabli vezanih za Playera
    protected override void Start () {
        this.transform.position = new Vector3(player.Position.x, player.Position.y, 0);
        levelText.text = "LV." + player.level; //grafički prikaz razine iskustva
        characterName.text = player.name; //grafički prikaz teksta
        numberDashes = player.dashIndicator; 
        healthUI.init(player.currentHealth,player.maxHealth); // inicijalizacija currenthp i hp za prikaz zdravlja preko UI
        dashUI.init(numberDashes, player.dashIndicator); // prikaz trenutnog stanja dash vještine
        _keepHodlerOfColldown = dashColldownTime;       
        base.Start();
    }
    /**
     * Poziva se svaki fiksni frameRate okvir
     * Dobra zbog toga što ne mijenja se s obzirom na brzinu izvođenja 
     */
    protected override void FixedUpdate () {
        PlayerControll();
        healthUI.init(player.currentHealth, player.maxHealth);
        dashUI.init(numberDashes, player.dashIndicator);
        levelText.text = "LV." + player.level;
        CheckDead();
        base.FixedUpdate();

        if (isDashing) {
            thisRigidbody.velocity = direction.normalized * dashSpeed;   
        }
        if (numberDashes < player.dashIndicator) {
            dashColldownTime -= Time.deltaTime;
           
            if (dashColldownTime < 0) {
                numberDashes++;
                dashColldownTime = _keepHodlerOfColldown;
            }
        }
    }
    //Inicijalizacija komponenti player kontejnera
    public void InitPlayer(Player play) {
        this.player.id = play.id;
        this.player.name = play.name;
        player.level = play.level;
        player.exp = play.exp;
        player.gold = play.gold;
        player.maxHealth = play.maxHealth;
        player.physicalDamage = play.physicalDamage;
        player.defense = play.defense;
        player.Position.x = play.Position.x;
        player.Position.y = play.Position.y;
        player.currentHealth = play.currentHealth;
        
    }
    //Kontrola playera  
    private void  PlayerControll() {
      
        direction = Vector2.zero; // početna vrijednost smjera je uvijek (0,0)
        moving = false; 
        // u slučaju da je igrač unutar dialoga ne dopustiti kretnje
        if (isTalking) {
            
            return;
        }   
        /*Kontrola preko ulaznih parametara tipkovnice
         * **/

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) {
            direction += Vector2.up;
            lastMove = Vector2.up;
            moving = true;

        }
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            direction += Vector2.down;
            lastMove = Vector2.down;
            moving = true;

        }
         if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            direction += Vector2.left;
            lastMove = Vector2.left;
            moving = true;

        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            direction += Vector2.right;
            lastMove = Vector2.right;
            moving = true;

        }
        if (Input.GetKeyDown(KeyCode.Space)) {

            if (!isAttacking)
            {
                weapon.SetActive(true);
                attackRoutine = StartCoroutine(SwordAttack()); 
            }
        }
        if (Input.GetKeyDown(KeyCode.LeftShift)){
            if (!isDashing)
            {
                _dashRoutine = StartCoroutine(Dash()); 
                
            }

        }
       
      
    }
    //provjerava se stanje playera
    private void CheckDead()
    {

        if (player.currentHealth <= 0)
        {

            player.dead = true;
            isDead = player.dead;
          


        }
    }
    //Regulator dash vještine
    private IEnumerator Dash() {

        if (numberDashes > 0 )
        {
            isDashing = true;
            moving = false;
            anim.SetBool("Dashing", isDashing);
            numberDashes--;
            //čeka se da dash time istekne
            yield return new WaitForSeconds(dashTime);
            //stopira se dash korutina
            StopDash();   
        }
        
    }
    //Zaustavlaj dash korutinu
    private void StopDash() {
        if (_dashRoutine != null)
        {
            StopCoroutine(_dashRoutine);
            
            isDashing = false;
            anim.SetBool("Dashing", isDashing);

        }
      
    }
    //Regulacija koliko štete igrač primi, amount označava vrijednost neprijateljea napada
    public override void TakeDamage(int amount)
    {
        if (player.currentHealth > 0)
        {           
            int damageTaken = amount-player.defense/2;
            // u slučaju da primljena šteta je manja od 10, postaviti je na 10 
            if (damageTaken < 10) {
                damageTaken = 10;
            }
            player.currentHealth -= damageTaken;
            base.TakeDamage(damageTaken);
           
        }
    }
   
    protected override void GameOver()
    {
        gameOver.SetActive(true);

        base.GameOver();
    }
    
    public void SaveSuccesfull()
    {
        saveSucc.SetActive(true);
    }
    public void SaveUnsucessfull()
    {
        saveSucc.SetActive(false);
    }

}
