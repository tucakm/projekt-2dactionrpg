﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
/**
 * Klasa za kontrolu enemy objekata
 * **/
public class EnemyController : EntityController
{
    // radijus detekcije igrača
    public float lookRadius = 15f;
    // vrijednost na kojoj nerijatelj staje 
    public float stopDistance = 3;  

    //referenca na UI od neprijatelja 
    private CanvasGroup _canvasGroup;
  
    //Zdravlje UI
    [SerializeField]
    private StatsController _enemyStatsUI;   
    //vrijednost za čekanje ponvonog napada   
    [SerializeField]
    private float _startAttackingTimer;
    //sprema vrijednost startAttackTimera
    private float _keepTimerAttack;

    //referneca na kontenjer o detaljnom opisu EnemyController klase
    public Enemy enemy;
        
    //Metoda se odvija prilikom prvog pokretanja. 
    protected override void Start()
    {
        enemy.dead = false;
        _canvasGroup = GetComponent<CanvasGroup>(); // dohvaća se CanvasGrupa za pristupu GUI Enemy
        enemy.currentHealth = enemy.maxHealth;
        _enemyStatsUI.init(enemy.currentHealth, enemy.maxHealth); //Inicijaliziraju se vrijednosti enemyHP, i enemy 
        _canvasGroup.alpha = 0; // postavljanje aktivnost GUI na neaktivno
        _keepTimerAttack = _startAttackingTimer;
        base.Start();

    }
    protected override void FixedUpdate()
    {
        EnemyMoving();
        _enemyStatsUI.init(enemy.currentHealth, enemy.maxHealth);
        CheckDead();
        base.FixedUpdate();
    }
    //Provjerava status neprijatelja
    private void CheckDead() {       
        if (enemy.currentHealth <= 0)
        {
          
            enemy.dead = true;
            enemy.deadInd = 1;
            isDead = enemy.dead;
            _canvasGroup.alpha = 0;
            
  
         }
    }
    //Unutar Unity Editora prikazuje redijus detekcije
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
    //Kretnje  
    private void EnemyMoving()
    {
        
        direction = Vector2.zero;
        moving = false;
        //Provjerava udaljenost igrača, da nije unutar stop distance ili izvan lookRadiusa
        if ((Vector2.Distance(PlayerController.playerInstance.transform.position, transform.position) > stopDistance) && (Vector2.Distance(PlayerController.playerInstance.transform.position, transform.position) <= lookRadius))
        {
            //Dodjeluje smjer 
            direction += (Vector2)(PlayerController.playerInstance.transform.position - transform.position);
            lastMove = direction;
            moving = true;

        }
        //Provjerava da li je igrač unutar stopDistance
        if ((Vector2.Distance(PlayerController.playerInstance.transform.position, transform.position) <= stopDistance))
        {
            //U slučaju da već ne nadpada pokrenuti napdad
            if (!isAttacking)
            {
                //Pokreće timer ponovnog napda
                _startAttackingTimer -= Time.deltaTime;
                if (_startAttackingTimer < 0)
                {
                    weapon.SetActive(true);
                    //poreče korutinu
                    attackRoutine = StartCoroutine(SwordAttack());
                    //postavlja timer na početnu vrijednost
                    _startAttackingTimer = _keepTimerAttack;
                }
                
            }
        }
    }
    // Metoda koja se pokreće unutar SwordAttackController skripte,
    // prima količinu štete upućene
    public override void TakeDamage(int amount) {

        if (enemy.currentHealth > 0)
        {
            // postavlja vrijednost enemy GUI na aktivno i pokazuje healthBar
            _canvasGroup.alpha = 1;
            // načinena šteta s obzirom na obranu 
            int damageTaken = amount - enemy.defense / 2; 
            enemy.currentHealth -= damageTaken;
            //poziva se base funkcia sa kreacijom grafičkih prikaza napdada,krvi, brojeva, 
            base.TakeDamage(damageTaken);

        }
       
    }  

    //Dodjeluje exp prilikom eminimacije ovog objekta,  poziva se unutar Enity ControllAnimation
    protected override void  EnemyExpGive() {
        //Dodjeljuje exp igraču
        PlayerController.playerInstance.player.CheckIfLevelUp(enemy.ExpGiving(PlayerController.playerInstance.player.level));
        //poziva base funkciju za izvršenje
        base.EnemyExpGive();
    }
    //Ažurira stanje EnemyQuestova
    protected override void EnemyQuestUpdate() {        
        QuestManager.instance.enemyKilled = enemy.enemyType;

        base.EnemyQuestUpdate();
    }
}
