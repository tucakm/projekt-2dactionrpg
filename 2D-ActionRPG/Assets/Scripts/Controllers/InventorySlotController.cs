﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/**
 *Klasa definira polja unutar inventorya 
 */
public class InventorySlotController : MonoBehaviour {

    public Item item; //referenca na item
    public Button removeButton; // referenca na tipku za ukloniti
    public Image icon; // referenca na ikonu

    //u slučaju postojkanja item objekta unutar ove skripte postaljvamo vrijednosti na dolje definirano.
    void Start () {
        if (this.item != null)
        {

            this.icon.sprite = this.item.icon;
            this.icon.enabled = true;
            this.removeButton.interactable = true;
        }
    }
   //Metoda za dodavanje itema, poziva se unutar InventoryUIControllera
    public void AddItem(Item newItem)
    {
        this.item = newItem;
        this.icon.sprite = this.item.icon;
        this.icon.enabled = true;
        removeButton.gameObject.SetActive(true);
        this.removeButton.interactable = true;
        
        
    }
    //Čišćenje polja prilikom korištenja itema
    public void ClearSlot() {
        this.item = null;
        this.icon.sprite = null;
        this.icon.enabled = false;
        removeButton.gameObject.SetActive(false);
        this.removeButton.interactable = false;
     
    }
    //U metoda za brizanje itema iz liste, poziva se klikom na tipku DELETE
    public void OnRemoveButton() {
        InventoryController.instance.Delite(this.item);
    }
    //Metoda za korištenje itema, poziva se klikom na polje unutar inventory-a koje sadrži objekt tipa item
    public void UseItem() {
        if (item != null)
        {

            if (item.Use())
            {               
            }
           
            
        }
    }
}
