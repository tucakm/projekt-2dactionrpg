﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Klasa zadužena za interakciju svih objekata unutar scene s bazom podatakak
// Zatim koristimo je za učitavanje scena 
// i služi kao glavni upravljač igre
public class SceenController : MonoBehaviour {

   
    public static SceenController sceeneInstance;  //Instanca samo može postojati 1
    private void Awake()
    {
        if (sceeneInstance == null)
        {
            DontDestroyOnLoad(gameObject); // omogućuje objetu da bude aktivan kroz cijelu igru
            sceeneInstance = this;
        }
        else {
            Destroy(gameObject);
        }
       
    }
    //lista neaktivnih neprijatelja
    public static List<GameObject> enemyDeadCount = new List<GameObject>();
    //lista neaktivnih itema 
    public static List<string> itemsPickedUP = new List<string>(); 
    //lista odrađenih questova
    public static List<string> questDone = new List<string>();
    // polje s trenutno stanjem questova
    public GameObject[] questsCheck;

    [SerializeField]
    private InputField playerName; // referenca na polje za unos    
    [SerializeField]
    private Button continueGame; // referenca na tipku countinue
    
    public PlayerController player; //prilikom ne pokretanje 2. scene u kojoj se glavni igrač koristi, punimo njega vrijednostima unutar ove reference. 

    

    private List<Item> item = new List<Item>(); // lista pročitanih itema iz baze
    private List<Enemy> ene = new List<Enemy>(); //lista pročitanih neprijatelja iz baze
    private List<QuestController> ques = new List<QuestController>(); //lista pročitanih questova iz baze
    private List<NPC> np = new List<NPC>(); // lista informacija o npc pročitanih iz baze
    private bool canQuit = true; // ne dopušta izlazak iz aplikacije prilikom spremanja u bazu

    private GameObject[] en; // dohvaćanje enemy objekta iz scene
    private GameObject[] items; // dohvačanje item objetka iz scnee
    private GameObject[] quests; // dohvaćanje quest objeta iz scene
    private GameObject[] npc; // dohvaćanje npc objekta iz scene

    private bool newGame; //flag da li je nova igra
    [TextArea]
    public string[] startText; // početni tekst prilikom prvog pokretanja igre,s novim likom
    
    void Start () {

  
        newGame = false;
        
        
    }
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
    //Metoda za učitavanje podataka čim se scena učita
    
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {

        if (scene.name == "MainScene")
        {
            if (DatabaseController.dbInstance.GetPlayer().id == 0)
            {

                continueGame.gameObject.SetActive(false);
            }
            else
            {

                continueGame.gameObject.SetActive(true);
            }
        }

        if (scene.name == "FirstScene") {
            //čitanje objektata unutar scene
            en = GameObject.FindGameObjectsWithTag("Enemy");
            items = GameObject.FindGameObjectsWithTag("Item");
            quests = GameObject.FindGameObjectsWithTag("Quest");
            SetInactive(quests);
            npc = GameObject.FindGameObjectsWithTag("NPC");
            
            //nova igra, nova zapisivanja unutar baze
            if (newGame)
            {
                player.InitPlayer(DatabaseController.dbInstance.GetPlayer());
                //ResetItems(items);
                DatabaseController.dbInstance.InsertIntoNPC(npc, player.player.id);
                DatabaseController.dbInstance.InsertQuests(quests, player.player.id);
                DatabaseController.dbInstance.InsertNewEnemies(en, player.player.id);
                DatabaseController.dbInstance.InsertNewItems(items, player.player.id);
                ques = DatabaseController.dbInstance.GetQuests(player.player.id);
                CheckQuestActive(quests);
                DialogController.dialogControllerInstance.arrayOfDialogs = startText;                
                DialogController.dialogControllerInstance.ShowDialog();
             
            }
            //nastavak igre, čitanje podataka iz baze
            else
            {
                np = DatabaseController.dbInstance.GetNPCInfo(player.player.id);
                item = DatabaseController.dbInstance.GetInactiveItems(player.player.id);                
                ene = DatabaseController.dbInstance.GetInactiveEnemies(player.player.id);
                ques = DatabaseController.dbInstance.GetQuests(player.player.id);
                CheckQuestActive(quests);
                CheckItemActive(items);
                CheckEnemyActive(en);
                SetValuesToNPC(npc);
               
                
            }
        }
  
    }
    //funkcija za postavljanje inaktivnih objekata
    private void SetInactive(GameObject[] obj)
    {
        for (int i = 0; i < obj.Length; i++) {
            obj[i].SetActive(false);
        }

    }
    //funkcija za resetanje itema
   /* private void ResetItems(GameObject[] ob) {
        ItemsController icont;
        for (int i = 0; i < ob.Length; i++) {
            icont = ob[i].GetComponent<ItemsController>();
            icont.ResetItems();
        } 
    }*/
    
    //Funkcija za provjeru aktivnosti itema s obzirom podatke iz baze
    private void CheckItemActive(GameObject[] obj) {
        foreach (var itm in item)
        { for (int i = 0; i < obj.Length; i++)
            {
                if (itm.itemName == obj[i].name)
                {
                    obj[i].gameObject.SetActive(false);
                    InventoryController.instance.UplodInventory(itm);
                }
            }
        }
    }
    //Funkcija za provjeru aktivnosti neprijatelja s obzirom podatke iz baze
    private void CheckEnemyActive(GameObject[] obj) {
        foreach (var e in ene)
        {
            for (int i = 0; i < obj.Length; i++)
            {
                if (e.name == obj[i].name)
                {                   
                    obj[i].gameObject.SetActive(false);
                }
            }
        }
    }
    //Funkcija za provjeru aktivnosti questova s obzirom podatke iz baze
    private void CheckQuestActive(GameObject[] obj) {
        ques.Reverse(); 
        QuestController qm = new QuestController();
        for (int i = 0; i < obj.Length; i++)
        {           
           foreach (var a in ques)
            {
                if (a.questName == obj[(obj.Length-1)-i].name)
                {                 
                    
                    obj[(obj.Length - 1) - i].gameObject.SetActive(true);
                    qm = obj[(obj.Length - 1) - i].GetComponent<QuestController>();
                    qm.enemyKillCount = a.enemyKillCount;
                }
               
            }
        }
    }
    //Postavlja vrijednost, questova za dovršiti unutar neprijatelja.
    private void SetValuesToNPC(GameObject[] obj) {
        NPCController ovajNPC;
        foreach (var n in np) {
            for (int i = 0; i < obj.Length; i++)
            {
                if (n.name == obj[i].name)
                {
                    ovajNPC= obj[i].GetComponent<NPCController>();
                    ovajNPC.InitValues(n.questsToCompliete);

                }
            }
        }
    }
    //Nova igra, ne dopušta početak ako je ime prazno
    public void NewGame() {
        if (playerName.text == "")
        {
            Debug.Log(playerName.text);
        }
        else {
           //kreira se novi igrač unutar baze
            DatabaseController.dbInstance.InsertNewPlayer(playerName.text.ToString());
            
            newGame = true;
            //pokreće se onSceneload metoda i učitava se nova scena
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1, LoadSceneMode.Single);                     
        }
    }
    //nastavak igre
    public void ContinueGame()
    {
        player.InitPlayer(DatabaseController.dbInstance.GetPlayer());
        //pokreće se onSceneload metoda i učitava se nova scena
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1, LoadSceneMode.Single);
      
    }  
   //metoda spremanja igre unutar baze, registracija prilikom pritiska na tipku save
    public void SaveGame()
    {
        canQuit = false;
        npc = GameObject.FindGameObjectsWithTag("NPC");
        questsCheck = GameObject.FindGameObjectsWithTag("Quest");
        for (int i = 0; i < questsCheck.Length; i++) {
            QuestController qc = questsCheck[i].GetComponent<QuestController>();
            DatabaseController.dbInstance.UpdateQuests(questsCheck[i], PlayerController.playerInstance.player.id,qc.enemyKillCount);
        }
             
        DatabaseController.dbInstance.UpdatePlayer(PlayerController.playerInstance);
        DatabaseController.dbInstance.UpdateQuestsDone(questDone, PlayerController.playerInstance.player.id);
        DatabaseController.dbInstance.UpdateNPC(npc, PlayerController.playerInstance.player.id);
        DatabaseController.dbInstance.UpdateEnemyActive(enemyDeadCount, PlayerController.playerInstance.player.id);
        DatabaseController.dbInstance.UpdateItemActive(itemsPickedUP, PlayerController.playerInstance.player.id);
        DatabaseController.dbInstance.UpdateItemUsed(InventoryController.instance.itemUsed,PlayerController.playerInstance.player.id);

        Debug.Log(InventoryController.instance.itemUsed);
        PlayerController.playerInstance.SaveSuccesfull();
        canQuit = true;
    }
    //izlaz iz aplikacije
    public void Quit()
    {
        if (canQuit)
        {
            Application.Quit();
        }

    }
  

}
