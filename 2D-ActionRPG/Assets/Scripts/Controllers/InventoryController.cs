﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryController : MonoBehaviour
{

    public static InventoryController instance;
    /*Može postojati samo jedna instanca inventory controller unutar igre
     */
    private void Awake()
    {
        if (instance != null)
        {           
            return;
        }
        instance = this;
    }
    //Lista itema unutar inventory-a
    public List<Item> items = new List<Item>();
    //Lista iskorišteni itema   
    public List<string> itemUsed = new List<string>();
    //delegate koristimo za prijenost metoda kao argumenata, drugim metodama.
    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    public int space = 25;//veličina inventorya
   
    //Dodavanje unutar inventorya
    public bool Add(Item item)
    {
        if (items.Count >= space) {
            Debug.Log("Nema mjesta");
            return false;
        }
        
        items.Add(item);
        
        if (onItemChangedCallback != null)
        {
             onItemChangedCallback.Invoke();
        }
        return true;
    }
    //Brisanje iz inventorya
    public void Delite(Item item)
    {
        itemUsed.Add(item.name);
        items.Remove(item);
        if (onItemChangedCallback != null)
        {
            onItemChangedCallback.Invoke();
        }     
    }
    //Prilikom učitavanja iz baze, dodavanje unutar inventorya
    public void UplodInventory(Item loadItems){
        Item novi=new Item();
        novi.Init(loadItems.itemName, loadItems.active, loadItems.used);
        if (novi.active == 0 & novi.used == 0) {
           if (!items.Contains(novi))
               {
                    Add(novi);
               }                  
        }
        if (onItemChangedCallback != null)
        {
            onItemChangedCallback.Invoke();
        }

    }
}
