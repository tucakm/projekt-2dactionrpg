﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUIController : MonoBehaviour {

    public Transform itemsParents; // referenca za pristup mjestima unutar 
    public GameObject inventoryUI; //referenca na inventory UI
    
    InventorySlotController[] slots;
	void Start () {
        
        slots = itemsParents.GetComponentsInChildren<InventorySlotController>();
        //postavljanje metode deligata
        InventoryController.instance.onItemChangedCallback += UpdateUI;
        UpdateUI();
    }
	//Otvaranje InventoryUI,
	void Update () {
        if (Input.GetKeyDown(KeyCode.I)) {
            //!inventoryUI.activeSelf mijenjanje iz aktivnog u pasivno stanje svakim pritiskom na tipku I
            inventoryUI.SetActive(!inventoryUI.activeSelf);
        }
       
    }
    //Dodavanje i brisanje iz UI, sve unutar funkcije je samo objašnjivo
    private void UpdateUI() {
     
        for (int i = 0; i < slots.Length; i++) {
            if (i < InventoryController.instance.items.Count)
            {
                slots[i].AddItem(InventoryController.instance.items[i]);
            }
            else {
                slots[i].ClearSlot();
            }
        }
    }
}
