﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/**
 * Klasa za kontrolu aktivnih questova
 * */
public class QuestController : MonoBehaviour {

    [SerializeField]
    private int questID;
    public string questName;
    public string startText;
    public string endText;

    public bool isEnemyQuest; // tip questa
    public string targetEnemyName; // ime mete
    public int enemiesToKill; 
   
    public int enemyKillCount;

    [SerializeField]
    private int expReward;
    [SerializeField]
    private int goldReward;

    public Text logQuestText; // tekst unutar liste aktivnih questova
    public Text killCountText; // broj ubijenih neprijatelja unutar liste aktivnih questova

   
    public NPCController npcCont; // referenca na nostielja questa
    public bool startedQuest; // flag za početak questa
	
	void Start () {
        questName = this.gameObject.name;
        logQuestText.gameObject.SetActive(true);
        killCountText.gameObject.SetActive(true);
        logQuestText.text = startText;               
        killCountText.text = enemyKillCount + "/" + enemiesToKill;


    }
	void Update () {
           
        if (isEnemyQuest) {
           
            if (enemyKillCount >= enemiesToKill) {
                EndQuest();
            }
            if (QuestManager.instance.enemyKilled == targetEnemyName)
            {
                QuestManager.instance.enemyKilled = null;
                enemyKillCount++;
                killCountText.text = enemyKillCount + "/" + enemiesToKill;

            }
            
        }
       
	}
    //početak questa
    public void StartQuest() {
        startedQuest = true;
        QuestManager.instance.enemyKilled = null;
        QuestManager.instance.ShowQuestText(startText);
    }
    //kraj questa
    public void EndQuest()
    {
        
        startedQuest = false;
        QuestManager.instance.ShowQuestText(endText); // poziva se dialog box s end textom      
        QuestManager.instance.questCompleted[questID] = true; // quest se ažurira da je dovršen
        PlayerController.playerInstance.player.CheckIfLevelUp(expReward); // dodavanje exp.a igraču
        PlayerController.playerInstance.player.gold += goldReward; // dodavanje novca
        npcCont.DoneQuest(); // ažurira se stanje questa kod npc-a
        logQuestText.text = "";
        killCountText.text = "";
        logQuestText.gameObject.SetActive(false);
        killCountText.gameObject.SetActive(false);
        SceenController.questDone.Add(this.gameObject.name); // spremanje podataka za ažuriranje unutar baze
        gameObject.SetActive(false);

    }

}
