﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*Klasa s koja kontrolira dijalog unutar igre 
 * **/
public class DialogController : MonoBehaviour {

    //instanca dostupna svima, samo jedna moguća u isto vrijeme kroz aplikaciju
    public static DialogController dialogControllerInstance; 
    //Izvodi se odmah pri kreaciji objekta
    private void Awake()
    {
       if (dialogControllerInstance == null)
       {

           dialogControllerInstance = this;
       }
       else
       {
           Destroy(gameObject);
       }

   }

    [SerializeField]
    private GameObject _dialogBox; // referenca na grafički prikaz dialog boxa
    [SerializeField]
    private Text _dialogText; // referenca na dialog text

    public bool dialogActive;
    [TextArea]
    public string[] arrayOfDialogs; // tekst što se prikazuje unutar dijalog boxa
      
    public int currentLine; // trenutni indeks teksta
    public bool startText; //da li je početni tekst pri prvom ulazu 

    //Upravlja tekstom unutar dialog boxa
    private void Update()
    {
        if (dialogActive)
        {
            PlayerController.playerInstance.isTalking = true;
        }
        
        if (dialogActive && Input.GetKeyDown(KeyCode.Space))
        {
            
            currentLine++;
           
        }
        
        if (currentLine >= arrayOfDialogs.Length)
        {
                     
                _dialogBox.SetActive(false);
                dialogActive = false;
           
                PlayerController.playerInstance.isTalking = false;
                
                currentLine = 0;
            
        }
      
        _dialogText.text = arrayOfDialogs[currentLine];
        
    }
    //Metoda za interakciju s ostalim likovima za prikaz dialog boxa
    public void ShowDialog() { 
        _dialogBox.SetActive(true);
        dialogActive = true;
      
    }
}
