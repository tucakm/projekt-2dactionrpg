﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/**
 * Klasa trenutno ne koristi nikakve metode it Entity klase, u budučnosti implementirati
 *  
 */
public class NPCController : EntityController {

   
    [TextArea] 
    public string[] dialogArray;  // tekst dialoga prilikom završetka svih questova kod npca 
    public int[] numberOfQuestHolding; // referenca na idquest, označuje koji quest npc sadrži  
    public NPC npc; // kontenjer podataka o npc-u

    protected override void Start () {
        npc.name = this.name; // dodjeljuje vrijednost definirana unutar Unity Enigna        
    }
	protected override void FixedUpdate () {
        //Projverava da li npc sadrži još questova
        if ((npc.totalQuests - npc.questsToCompliete) == npc.totalQuests) {
            npc.holdingQuest = false;
        }
	}
    //Završava quest
   public void DoneQuest() {
        if (npc.questsToCompliete != 0)
        {
            numberOfQuestHolding[npc.totalQuests - npc.questsToCompliete] = 9999;
            npc.questsToCompliete--;
        }                  
    }
    //Metoda iniciranje vrijednosti questToCompliete
    public void InitValues(int questsToCompliete) {
        this.npc.questsToCompliete = questsToCompliete;
    }
   
}
