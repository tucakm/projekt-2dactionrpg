﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/**
 *Klasa zadužena za upravljanje kolizijom mača  
 */
public class SwordAttackController : MonoBehaviour {

    //Referenca na EnemyController objekt
    private EnemyController _enemyControler; 
    //Kolizije omogućiti svako određeno vrijeme, to reguliramo s ove 3 varijable
    private float _timeToWaitBettwenCollisionDetects; 
    private float _saveTime; 
    private bool _collisionDetect;  //omogućava koliziju 

    void Start()
    {     
        _saveTime = _timeToWaitBettwenCollisionDetects;       
        _timeToWaitBettwenCollisionDetects = PlayerController.playerInstance.swordAttackTime; // pridjeluje se vrijednost čekanja napada, za detekciju kolizija
        _collisionDetect = true;      
    }

    void Update()
    { 
        //Timer za omogućavanje ponovne kolizije
        if (!_collisionDetect) {
            _timeToWaitBettwenCollisionDetects -= Time.deltaTime;
            if (_timeToWaitBettwenCollisionDetects <= 0) {
                _timeToWaitBettwenCollisionDetects = _saveTime;
                _timeToWaitBettwenCollisionDetects = PlayerController.playerInstance.swordAttackTime;
                _collisionDetect = true;
            }
        }
       
    }

    //Detekcija kolizije preko colision2D komponente
    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Player" && _collisionDetect)
        {
            _enemyControler = gameObject.transform.root.GetComponent<EnemyController>();        
            PlayerController.playerInstance.TakeDamage(_enemyControler.enemy.physicalDamage); 
            _collisionDetect = false;
        }
       
    }
    //Detekcija kolizije preko colsions2d komponente, koristeći trigger
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy" && _collisionDetect)
        {
            _enemyControler=collision.gameObject.GetComponent<EnemyController>();
            _enemyControler.TakeDamage(PlayerController.playerInstance.player.physicalDamage);
            _collisionDetect = false;

        }

    }



}
