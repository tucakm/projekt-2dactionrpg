﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Klasa za upravljanje objekta kamere unutar Unity-a
 * Prati PlayerController instancu
 * */
public class CameraControler : MonoBehaviour {

  
    public float moveSpeed; // brzina praćenja 
    public BoxCollider2D boundOfCamera; // referenca na BoxColider postavljen oko mape, za limitaciju kretnje kamere

    private Vector3 _minBounds; 
    private Vector3 _maxBounds; 
    private Vector3 _targetPosition; 
    private Camera _theCamera;
    private float _halfHeight;
    private float _halfWidth;

    private void Start()
    {
        // minimalne granice box colidera
        _minBounds = boundOfCamera.bounds.min;
        // maksimalne granice box colidera
        _maxBounds = boundOfCamera.bounds.max;

        //Dohvaćanje komponente Camera iz UnityEditora
        _theCamera = GetComponent<Camera>();

        //Dohvaćanje dužine prikaza kamere 
        _halfHeight = _theCamera.orthographicSize;
        //Prilagodba prikaza širine za sve veličine monitora
        _halfWidth = (_halfHeight * Screen.width) / Screen.height;
    }

    void Update () {
        //Dohvaćanje trenutne pozicije PlayerControllera, s tim da ne mijenjamo vrijednost z varijable, jer ne želimo da kamera se spusti na PlayerControllera vrijednost z varijable (izbočenost)
        _targetPosition = new Vector3(PlayerController.playerInstance.transform.position.x, PlayerController.playerInstance.transform.position.y, transform.position.z);
        //Lerp omogučuje prijelaz iz jedne Vector3 vrijednosti u drugu, kroz određeno vrijeme. Dobijemo dojam laganog pomaka kamere.
        transform.position = Vector3.Lerp(transform.position, _targetPosition, moveSpeed*Time.deltaTime);


        //Mathf.Clamp (početna vrijednost,donja granica,gornja granica) steže vrijednost unutar granica 
        //Kontrola vrijednosti pozicije, da su unutar granica širine
        float clampX = Mathf.Clamp(this.transform.position.x, _minBounds.x + _halfWidth, _maxBounds.x - _halfWidth);
        //Kontrola vrijednosti pozicije, da su unutar granica dužine
        float clampY = Mathf.Clamp(this.transform.position.y, _minBounds.y + _halfHeight, _maxBounds.y - _halfHeight);

        //postavlja lokaciju kamere na novo izračunate vrijednosti
        transform.position = new Vector3(clampX, clampY, transform.position.z);
    }
}
