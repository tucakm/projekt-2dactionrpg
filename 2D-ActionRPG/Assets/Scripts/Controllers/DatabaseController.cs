﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Data;
using Mono.Data.Sqlite;
//Klasa zadužena za komunikaciju s bazom
public class DatabaseController : MonoBehaviour
{
    //konekcija
    private string conn;
    //instanca
    public static DatabaseController dbInstance;
    //naziv baze
    protected const string DATABASE_NAME = "ActionRPG.db";

    private void Awake()
    {
        dbInstance = this;
        conn = "URI=file:" + Application.dataPath + "/" + DATABASE_NAME;
        CreateTable();

    }
    //čisti otvorene veze
    private void OnDestroy()
    {
        Mono.Data.Sqlite.SqliteConnection.ClearAllPools();
    }
    //kreiranje tablice
    private void CreateTable()
    {

        //provjeravanje da li postoji vec
        if (!System.IO.File.Exists(Application.dataPath + "/" + DATABASE_NAME))
        {
           

            if (System.IO.File.Exists(Application.streamingAssetsPath + "/" + DATABASE_NAME))
            {
                
                System.IO.File.Copy(Application.streamingAssetsPath + "/" + DATABASE_NAME, Application.persistentDataPath + "/" + DATABASE_NAME, true);
            }
            else
            {
                Debug.Log("ERROR: the file DB named " + (Application.streamingAssetsPath + "/" + DATABASE_NAME) + " doesn't exist in the StreamingAssets Folder, please copy it there.");
            }
        }
        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                string sqlQuery = String.Format("CREATE TABLE IF NOT EXISTS Player ( player_id INTEGER PRIMARY KEY AUTOINCREMENT, player_name VARCHAR ( 255 ) NOT NULL, player_level INTEGER DEFAULT 1, player_exp INTEGER DEFAULT 0, player_gold INTEGER DEFAULT 0, player_hp INTEGER DEFAULT 150, player_attack INTEGER DEFAULT 40, player_def INTEGER DEFAULT 20, player_location_x FLOAT DEFAULT -40.14, player_location_y FLOAT DEFAULT -14.34, player_currenthp INTEGER DEFAULT 150 );" +
                    "CREATE TABLE IF NOT EXISTS Items ( item_id INTEGER PRIMARY KEY AUTOINCREMENT, item_name VARCHAR ( 255 ) NOT NULL,item_active INT DEFAULT 1, item_postion_x NUMERIC, item_position_y INTEGER, item_used INTEGER DEFAULT 0, player_id INTEGER,item_icon	TEXT ); " +
                   "CREATE TABLE IF NOT EXISTS Enemy (enemy_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, enemy_name TEXT, enemy_type TEXT DEFAULT En, enemy_dead INTEGER NOT NULL DEFAULT 0, player_id INTEGER );"+
                   "CREATE TABLE IF NOT EXISTS NPC ( npc_id INTEGER PRIMARY KEY AUTOINCREMENT, npc_name TEXT NOT NULL, npc_holdingQuest INTEGER DEFAULT 1, npc_totalQuests INTEGER, npc_QuestToCompliete INTEGER, player_id INTEGER );"+
                   "CREATE TABLE IF NOT EXISTS Quests ( quest_id INTEGER PRIMARY KEY AUTOINCREMENT, quest_name TEXT, quest_active INTEGER DEFAULT 0, quest_done INTEGER DEFAULT 0, quest_enemyKilled INTEGER DEFAULT 0, npc_name INTEGER, player_id INTEGER );");

                dbCmd.CommandText = sqlQuery;
                dbCmd.ExecuteScalar();
                dbCmd.Dispose();
            }
            dbConnection.Dispose();
        }
    }
    //INSERT, SELECT I UPDATE za igrača
    #region Player 
    public bool InsertNewPlayer(string name)
    {
        bool isNewInserted = false;
        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                Debug.Log("INFO: InsertNewPlayer on database in path " + conn);
                string sqlQuery = String.Format("INSERT INTO Player(player_name) VALUES(\"{0}\");", name);
                dbCmd.CommandText = sqlQuery;
                dbCmd.ExecuteScalar();
                isNewInserted = true;
                dbCmd.Dispose();
            }
            dbConnection.Dispose();

        }
        return isNewInserted;
    }

    public Player GetPlayer()
    {
        Player player = new Player();
        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {

                //Debug.Log("INFO: GET PLAYER on database in path " + conn);
                string sqlQuery = "SELECT * FROM Player WHERE player_id=(SELECT MAX(player_id) FROM Player)";
                dbCmd.CommandText = sqlQuery;

                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        player.id = reader.GetInt32(0);
                        player.name = reader.GetString(1);
                        player.level = reader.GetInt32(2);
                        player.exp = reader.GetInt32(3);
                        player.gold = (Int32)reader.GetInt64(4);
                        player.maxHealth = (Int32)reader.GetInt64(5);
                        player.physicalDamage = (Int32)reader.GetInt64(6);
                        player.defense = (Int32)reader.GetInt64(7);
                        player.Position.x = reader.GetFloat(8);
                        player.Position.y = reader.GetFloat(9);
                        player.currentHealth = (Int32)reader.GetInt64(10);
                    }
                   
                    reader.Close();
                    dbCmd.Dispose();
                }               
                dbConnection.Dispose();
            }
        }

        return player;
    }

    public void UpdatePlayer(PlayerController pl)
    {

        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                Debug.Log("INFO: Update Player on database in path " + conn);

                string sqlQuery = String.Format("UPDATE PLAYER SET " +
                    "player_level=\"{1}\"," +
                    "player_exp=\"{2}\"," +
                    "player_gold=\"{3}\"," +
                    "player_hp=\"{4}\"," +
                    "player_attack=\"{5}\"," +
                    "player_def=\"{6}\"," +
                    "player_location_x=\"{7}\"," +
                    "player_location_y=\"{8}\"," +
                    "player_currenthp=\"{9}\"" +
                    " WHERE player_id=\"{0}\"", pl.player.id, pl.player.level, pl.player.exp, pl.player.gold, pl.player.maxHealth, pl.player.physicalDamage, pl.player.defense, pl.transform.position.x, pl.transform.position.y, pl.player.currentHealth);

                dbCmd.CommandText = sqlQuery;
                dbCmd.ExecuteScalar();
            }
            dbConnection.Dispose();
        }
    }
    public void ResetPlayerData(string name)
    {

        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                Debug.Log("INFO: Reset Player Data on database in path " + conn);

                string sqlQuery = String.Format("UPDATE PLAYER SET " +
                    "player_level=\"{1}\"," +
                    "player_exp=\"{2}\"," +
                    "player_gold=\"{3}\"," +
                    "player_hp=\"{4}\"," +
                    "player_attack=\"{5}\"," +
                    "player_def=\"{6}\"," +
                    "player_location_x=\"{7}\"," +
                    "player_location_y=\"{8}\"," +
                    "player_currenthp=\"{9}\"," +
                    "player_name=\"{10}\"" +
                    " WHERE player_id=\"{0}\"", 4, 1, 0, 0, 150, 50, 20, -40.14, -14.34, 150, name);

                dbCmd.CommandText = sqlQuery;
                dbCmd.ExecuteScalar();
                dbCmd.Dispose();
            }
            dbConnection.Dispose();
        }
    }
    #endregion
    //INSERT, SELECT I UPDATE  za iteme
    #region Items
    public List<Item> GetInactiveItems(int playerID)
    {
        List<Item> itm = new List<Item>();
        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {

               

                string sqlQuery = String.Format("SELECT * FROM Items WHERE item_active=\"{0}\" AND Items.player_id=\"{1}\";", 0, playerID);
                dbCmd.CommandText = sqlQuery;

                using (IDataReader reader = dbCmd.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        Item item = new Item();
                        item.item_id = reader.GetInt32(0);
                        item.itemName = reader.GetString(1);
                        item.active = reader.GetInt32(2);
                        item.used = reader.GetInt32(5);
                        Debug.Log(item.itemName + ":" + item.used+"id:"+ item.item_id);
                        itm.Add(item);
                    }
                    reader.Close();


                }
                dbCmd.Dispose();
            }
            dbConnection.Dispose();
        }
        return itm;
    }

    public void InsertNewItems(GameObject[] it, int playerID)
    {
        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                
                ItemsController items;
                for (int i = 0; i < it.Length; i++)
                {
                    items=it[i].GetComponent<ItemsController>();
                 
                    string sqlQuery = String.Format("INSERT INTO Items(item_name,item_active,item_postion_x,item_position_y,item_used,item_icon,player_id)" +
                        " VALUES(\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\");", items.name, items.item.active, items.transform.position.x, items.transform.position.y,items.item.used,items.item.icon,playerID);
                    dbCmd.CommandText = sqlQuery;
                    dbCmd.ExecuteScalar();

                }

                dbCmd.Dispose();

            }
            dbConnection.Dispose();
        }
    }
    public void UpdateItemActive(List<String> items, int playerID)
    {
        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                Debug.Log("INFO: Update Item Active on database in path " + conn);

                foreach (var ite in items)
                {

                    string sqlQuery = String.Format("UPDATE Items SET " +
                   "item_active=\"{1}\"" +
                   "WHERE item_name=\"{0}\"AND player_id=\"{2}\"", ite, 0, playerID);

                    dbCmd.CommandText = sqlQuery;
                    dbCmd.ExecuteScalar();

                }
                dbCmd.Dispose();

            }
            dbConnection.Dispose();
        }
    }
    public void UpdateItemUsed(List<String> items, int playerID)
    {
        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
               
                foreach (var ite in items)
                {
                    Debug.Log(ite);
                    string sqlQuery = String.Format("UPDATE Items SET item_used=\"{0}\"" +
                   "WHERE item_name=\"{1}\"AND player_id=\"{2}\"", 1, ite, playerID);
                    Debug.Log(sqlQuery);
                    dbCmd.CommandText = sqlQuery;
                    dbCmd.ExecuteScalar();

                }
                dbCmd.Dispose();

            }
           dbConnection.Dispose();
        }
    }
    #endregion
    //INSERT, SELECT I UPDATE  za enemy
    #region Enemy   
    public List<Enemy> GetInactiveEnemies(int playerID)
    {
        List<Enemy> enem = new List<Enemy>();
        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {

                
                string sqlQuery = String.Format("SELECT * FROM Enemy WHERE enemy_dead=\"{0}\" AND Enemy.player_id=\"{1}\";", 1, playerID);
                dbCmd.CommandText = sqlQuery;

                using (IDataReader reader = dbCmd.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        Enemy enemy = new Enemy();

                        enemy.id = reader.GetInt32(0);
                        enemy.name = reader.GetString(1);
                        enemy.enemyType = reader.GetString(2);
                        enemy.deadInd = reader.GetInt32(3);
                        enem.Add(enemy);
                    }
                    reader.Close();
                    dbCmd.Dispose();

                }
                dbConnection.Dispose();
            }
        }
        return enem;
    }

    public void InsertNewEnemies(GameObject[] enem, int playerID)
    {
        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                Debug.Log("INFO: InsertNewEnemies on database in path " + conn);
                for (int i = 0; i < enem.Length; i++)
                {
                    string sqlQuery = String.Format("INSERT INTO Enemy(enemy_name,player_id)" +
                        " VALUES(\"{0}\",\"{1}\");", enem[i].name, playerID);
                    dbCmd.CommandText = sqlQuery;
                    dbCmd.ExecuteScalar();

                }
                dbCmd.Dispose();

            }
            dbConnection.Dispose();
        }
    }
    public void UpdateEnemyActive(List<GameObject> enemy, int playerID)
    {

        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {

                

                foreach (GameObject en in enemy)
                {
                    string sqlQuery = String.Format("UPDATE ENEMY SET " +
                       "enemy_dead=\"{1}\"" +
                       "WHERE enemy_name=\"{0}\"AND player_id=\"{2}\"", en.name, 1, playerID);

                    dbCmd.CommandText = sqlQuery;
                    dbCmd.ExecuteScalar();

                }
                dbCmd.Dispose();
            }
            dbConnection.Dispose();
        }
    }
    #endregion Enemy
    //INSERT, SELECT I UPDATE  za questove
    #region Quests
    public void InsertQuests(GameObject[] obj, int playerID)
    {
        
        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                QuestController questCont;
                for (int i = 0; i < obj.Length; i++)
                {
                    questCont = obj[(obj.Length-1)-i].GetComponent<QuestController>();

                    string sqlQuery = String.Format("INSERT INTO Quests(quest_name,quest_active,quest_done,player_id)" +
                        " VALUES(\"{0}\",\"{1}\",\"{2}\",\"{3}\");", questCont.name,0,0, playerID);
                    dbCmd.CommandText = sqlQuery;
                    dbCmd.ExecuteScalar();

                }

                dbCmd.Dispose();

            }
            dbConnection.Dispose();
        }
    }
    public List<QuestController> GetQuests(int playerID)
    {
        List<QuestController> quests = new List<QuestController>();
        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {                

                string sqlQuery = String.Format("SELECT * FROM Quests WHERE player_id=\"{0}\" AND quest_active=\"{1}\" AND quest_done=\"{2}\";", playerID,1,0);
                dbCmd.CommandText = sqlQuery;

                using (IDataReader reader = dbCmd.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        QuestController q = new QuestController();
                        q.questName = reader.GetString(1);
                        q.enemyKillCount = reader.GetInt32(4);
                        quests.Add(q);
                    }
                    reader.Close();


                }
                dbCmd.Dispose();
            }
        }

        quests.Reverse();
        return quests;
    }
    public void UpdateQuests(GameObject ob, int playerID, int enemyKillCount)
    {

        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {


                string sqlQuery = String.Format("UPDATE Quests SET quest_active=\"{0}\",quest_enemyKilled=\"{3}\" WHERE player_id=\"{1}\" AND quest_name=\"{2}\";", 1, playerID, ob.name, enemyKillCount);
                dbCmd.CommandText = sqlQuery;
                dbCmd.ExecuteScalar();
                dbCmd.Dispose();



            }
            dbConnection.Dispose();
        }
    }
    public void UpdateQuestsDone(List<string> obj, int playerID)
    {

        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                if (obj != null)
                {
                    foreach (var o in obj)
                    {
                        Debug.Log(o);
                        Debug.Log(dbCmd.ToString());
                            string sqlQuery = String.Format("UPDATE Quests SET quest_active=\"{0}\" WHERE player_id=\"{1}\" AND quest_name=\"{2}\";", 0, playerID, o);
                            dbCmd.CommandText = sqlQuery;
                            dbCmd.ExecuteScalar();
                         
                        
                    }

                }
                dbCmd.Dispose();
                
            }
            dbConnection.Dispose();
        }
    }

    #endregion Quests
    //INSERT, SELECT I UPDATE  za NPC
    #region NPC 
    public List<NPC> GetNPCInfo(int playerID)
    {
        List<NPC> nps = new List<NPC>();
        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {

                string sqlQuery = String.Format("SELECT * FROM NPC WHERE player_id=\"{0}\";", playerID);
                dbCmd.CommandText = sqlQuery;

                using (IDataReader reader = dbCmd.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        NPC q = new NPC();
                        q.name= reader.GetString(1);
                        q.questsToCompliete = reader.GetInt32(4);
                        nps.Add(q);
                    }
                    reader.Close();


                }
                dbCmd.Dispose();
            }
            dbConnection.Dispose();
        }
        return nps;
    }

    public void InsertIntoNPC(GameObject[] obj, int playerID)
    {
        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                NPCController npcController;
                for (int i = 0; i < obj.Length; i++)
                {
                    npcController = obj[i].GetComponent<NPCController>();

                    string sqlQuery = String.Format("INSERT INTO NPC(npc_name,npc_totalQuests,npc_QuestToCompliete,player_id)" +
                        " VALUES(\"{0}\",\"{1}\",\"{2}\",\"{3}\");", npcController.name,npcController.npc.totalQuests,npcController.npc.questsToCompliete, playerID);
                    dbCmd.CommandText = sqlQuery;
                    dbCmd.ExecuteScalar();

                }
                dbCmd.Dispose();
            }
            dbConnection.Dispose();
        }
    }
    public void UpdateNPC(GameObject[] obj, int playerID) {

        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                NPCController npcController;
                for (int i = 0; i < obj.Length; i++)
                {
                    npcController = obj[i].GetComponent<NPCController>();

                    string sqlQuery = String.Format("UPDATE NPC SET npc_QuestToCompliete=\"{0}\" WHERE player_id=\"{1}\" AND npc_name=\"{2}\";", npcController.npc.questsToCompliete, playerID,npcController.name);
                    dbCmd.CommandText = sqlQuery;
                    dbCmd.ExecuteScalar();

                }
                dbCmd.Dispose();
            }
            dbConnection.Dispose();
        }
    }
    #endregion


}


