﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegisterDialogController : MonoBehaviour {

    [SerializeField]
    private NPCController npcController; //referenca na NPC Controller
  
    [SerializeField]
    private GameObject pressFtoTalk;   // referenca na UI text press f to talk    
  
    void Start () {          
       
        pressFtoTalk.SetActive(false);
       
    }
    private void Update()
    {
      

    }
    private void OnTriggerStay2D(Collider2D collision)
    {
       
        if (collision.gameObject.name == "Player") 
        {
            if (Input.GetKeyUp(KeyCode.F))
            {
               //u slučaju da dialog box nije aktivan
                if (!DialogController.dialogControllerInstance.dialogActive)
                {
                    //i npc s kojim vodimo interakciju sadrži quest
                    if (npcController.npc.holdingQuest)
                    {    
                        //predaje se quest number preko polja questova što npc sadrži, dok npc.totalquests-questToComplitete prestavljaju indeks
                        QuestManager.instance.questHolderNumber = npcController.numberOfQuestHolding[npcController.npc.totalQuests - npcController.npc.questsToCompliete];
                        // quest se postavlja na aktivno stanje
                        QuestManager.instance.quests[QuestManager.instance.questHolderNumber].gameObject.SetActive(true);
                        //dodaje se listi aktivnih questova
                        QuestManager.instance.questActive.Add(QuestManager.instance.quests[QuestManager.instance.questHolderNumber].gameObject);
                        //pokretanje questa
                        QuestManager.instance.quests[QuestManager.instance.questHolderNumber].StartQuest();
                        

                    }
                    //suprotnom čita se predefinirani npcdialog
                    else {                        
                        DialogController.dialogControllerInstance.currentLine = 0;
                        DialogController.dialogControllerInstance.arrayOfDialogs = npcController.dialogArray;
                        DialogController.dialogControllerInstance.ShowDialog();
                       
                    }
                    


                }
                
            }
        }
    }
    //U slučaju ulaza unutar colision colidera
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player") {
            pressFtoTalk.SetActive(true);
        }
        

    }
    //Slučaj izlaza iz colision colidera
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player") {
            pressFtoTalk.SetActive(false);          
        }


    }
}
