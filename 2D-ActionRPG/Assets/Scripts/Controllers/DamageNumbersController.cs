﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Grafički prikaz načinjene štete, 
 * */
public class DamageNumbersController : MonoBehaviour {

    [SerializeField]
    private float _moveSpeed; 
    [SerializeField]
    private Text _displayNumber; 
    public int damageNumber;
    
	void Start () {
		
	}
	
	
	void Update () {
        _displayNumber.text=""+ damageNumber;
        //Prikazani brojevi se lagano kreću prema gore
        transform.position = new Vector3(transform.position.x, transform.position.y + (_moveSpeed * Time.deltaTime), transform.position.z);
	}
}
