﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*Klasa koja upravlja questovima 
 */
public class QuestManager : MonoBehaviour {

  
 #region Singelton
    public static QuestManager instance; //instanca QuestManagera, može postojati samo 1 kroz igru

    private void Awake()
    {
        if (instance != null)
        {
         
            return;
        }
        instance = this;
    }
    #endregion

    //lista questova unutar igre
    public QuestController[] quests;
    //referenca na Listu Aktivnih questova UI
    public GameObject questLogUI;

    public ArrayList questActive = new ArrayList();
    //lista završenih questova
    public bool[] questCompleted;
    //zadnji ubijeni neprijatelj
    public string enemyKilled;
    // referenca na nositelja zadatka
    public int questHolderNumber;



    void Start()
    {       
        questCompleted = new bool[quests.Length];        
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            questLogUI.SetActive(!questLogUI.activeSelf);
        }
    }
    //Poziv teksta questa preko dialog boxa
    public void ShowQuestText(string questText)
    {
        DialogController.dialogControllerInstance.arrayOfDialogs = new string[1];
        DialogController.dialogControllerInstance.arrayOfDialogs[0] = questText;
        DialogController.dialogControllerInstance.currentLine = 0;
        DialogController.dialogControllerInstance.ShowDialog();
    }

}
