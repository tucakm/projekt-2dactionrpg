﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 *Abstraktna klasa koja definira osnovne kontrole upravljivih objekata unutar projekta
 *Zahtjeva da svaki objekt ima Rigidbody2D i Animator komponente 
 */
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public abstract class EntityController : MonoBehaviour {

     /**
     *[SerializeField] dopušta da privatne varijable budu dostupne unutar Unity Editor/Inspector
     */

    //Kretnje
    [SerializeField]
    private float _speed; // brzina kretnje 
    protected bool moving = false; 
    protected Vector2 direction; //smjer kretnje 
    protected Rigidbody2D thisRigidbody; // referenca na fizićku komponentu objekta
     
    //Animacije    
    [SerializeField]
    private float _animationDeadTimer; //duljina trajanja animacije smrti
    protected Vector2 lastMove; // Zadnji smjer zabilježen, normalizirana varijabla x(-1,0,1), y(-1,0,1) 
    protected Animator anim; //referenca na animator
    protected bool isDead; //flag za aktivaciju "Dead" animacije
    protected bool isDashing; // flag za aktivaciju "Dash" animacije 
    protected bool isAttacking; //flas za aktivaciju "SwordAttack" animacije 

    [SerializeField]
    public float swordAttackTime; //vrijeme čekanja između animacija

    [SerializeField]
    protected GameObject weapon; // referenenca na oružje     
  
    public GameObject _damageNumber; // referenca na objekt zadužen za prikazivanje štete   
    public GameObject _hitAnim; // referenca na hit/slash animaciju
    public GameObject bloodParticle; //referenca na prikaz krvi
          
    protected Coroutine attackRoutine; //refrerenca na Corutinu za napad 

    protected virtual void Start () {
        thisRigidbody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        weapon.SetActive(false);  //za svaki slućaj deaktivacija Weapon objekta (bolje da nije aktivan ako se ne napda)
     }
	/**
     * Poziva se svaki fiksni frameRate okvir
     * Dobra zbog toga što ne mijenja se s obzirom na brzinu izvođenja 
     * Ako se koristi RigidBody komponenta, proporučljiva za koristit
     */
	protected virtual void FixedUpdate ()
    {
        Move();
     }
    /**
     *Kretnje objekata
     */
    private void Move()
    {
        thisRigidbody.velocity = direction.normalized * _speed ;
        
        ControllAnimationLayers();
    }
   /**
    * Upravlja animacijama objekata, pozivajući layere i predavajući određene parametre 
    * Unity Editoru/Animatoru 
    */
    private void ControllAnimationLayers() {
        if (moving)
        {
            MovementAnimation(direction);
            ActivateLayer("MovingLayer");
          

        }
        else if(direction.x==0 && direction.y ==0){
            MovementAnimation(direction);
            moving = false;
            ActivateLayer("BaseLayer");
            
        }

        if(isAttacking)
        {
            moving = false;
            thisRigidbody.velocity = Vector2.zero;  // brzina na 0 da nema kretnje objekta ako objekt napada
            ActivateLayer("SwordAttacking");
            

        }
        if (isDead)
        {            
            anim.SetBool("Dead", isDead);
            ActivateLayer("DeadLayer");          

            _animationDeadTimer -= Time.deltaTime; //Hardcodano fixno vrijeme trajanja animacije dead
            thisRigidbody.velocity = Vector2.zero; // brzina na 0 da nema kretnje objekta 
            if (_animationDeadTimer < 0)
            {  
                

                if (this.gameObject.tag == "Player") {
                    GameOver(); //pokrećemo GameOver prozor
                    Time.timeScale = 0f; //zamrznemo svu fiziku unutar Scene
                    
                }
                if (this.gameObject.tag == "Enemy")
                {
                    //vrijedi samo za Tipa Entity/Enemy objekte
                    EnemyQuestUpdate();  // ažurira status zadatka
                    EnemyExpGive();   // Daje exp igraču
            
                    SceenController.enemyDeadCount.Add(this.gameObject); //dodaje unutar listu za ukupan broj mrtvih neprijatelja 

                    this.gameObject.SetActive(false); //Postavljamo objekt da je inaktivan 
                }
                                          
            }
          
        }
        //Vještina samo za igrača 
        if (isDashing) {
            ActivateLayer("DashLayer");
        }
    }

    /**
     *Animacija kretnji 
     */
    //postavljanje parametara unutar Animator komponente
    private void MovementAnimation(Vector2 direction) {
        
        anim.SetBool("Moving", moving); 
        anim.SetFloat("MoveX", direction.x); 
        anim.SetFloat("MoveY", direction.y);
        anim.SetFloat("LastMoveX", lastMove.x); 
        anim.SetFloat("LastMoveY", lastMove.y);
        
    }

    private void ActivateLayer(string layerName) {
       
        //Deaktivacija sivh aktivnih layera 
        for (int i = 0; i < anim.layerCount; i++) {         
                anim.SetLayerWeight(i, 0);                    
                     
        }
        //Aktivacija traženog layera
        anim.SetLayerWeight(anim.GetLayerIndex(layerName), 1);
    }

    //Metoda IEnumerator dopušta pokretanje zasebne pozadinske korutine, koja se odvija zasebno o fixedUpdate loop funckiji. 
    // stoga možemo postaviti vrijednost čekanja bez da zaštopamo igru
    //Metoda izvodi napad mačem
    protected IEnumerator SwordAttack()
    {
        isAttacking = true;
        anim.SetBool("SwordAttacking", isAttacking);
        yield return new WaitForSeconds(swordAttackTime); 
        StopAttack();
    }
    // Zaustavlja korutinu napda mačem 
    private void StopAttack()
    {
        if (attackRoutine != null)
        {            
            StopCoroutine(attackRoutine);
            weapon.SetActive(false);
            isAttacking = false;
            anim.SetBool("SwordAttacking", isAttacking);

        }
    }
    //Take damgae metoda koja se nasljeđuje, veže se na PlayerController a poziva se unutar SwordAttackController klase
    public virtual void TakeDamage(int amount)
    {           
            var clone = (GameObject)Instantiate(_damageNumber,this.transform.position, Quaternion.Euler(Vector3.zero));
            clone.GetComponent<DamageNumbersController>().damageNumber = amount;
            clone.transform.position = new Vector3(this.transform.position.x, this.transform.position.y+3, this.transform.position.z);
            var hitS = (GameObject)Instantiate(_hitAnim, this.transform.position, Quaternion.Euler(Vector3.zero));
            var blood = (GameObject)Instantiate(bloodParticle, this.transform.position, Quaternion.Euler(Vector3.zero));

    }
    //Metoda koja se veže na EnemyController klasu, dodjeljuje exp igraču
    protected virtual void EnemyExpGive() {
    }
    //Ažurira status zadatka, veže se na EnemyControleller klasu
    protected virtual void EnemyQuestUpdate()
    {

    }
    //Registrira status razine zdravlja igrača, parametri se dodjeljuje unutar PlayerController klase
    protected virtual void GameOver()
    {

    }
}
