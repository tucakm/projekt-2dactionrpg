﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//Grafički prikazuje zdravlje igrača i neprijatelja
public class StatsController : MonoBehaviour {

    //brzina mijenjanja vrijednosti healtha unutar UI
    [SerializeField]
    private float lerpSpeed;
    //referenca na sliku 
    private Image content;

    private float currentFill;
    [SerializeField]
    private Text healthText;

    public float MaxValue { get; set; }
    private float currentValue;
    //trenutna vrijednost zdravlja
    public float CurrentValue
    {
        get
        {
            return currentValue;
        }
        set
        {
            if (value > MaxValue)
            {
                currentValue = MaxValue;
            }
            else if (value < 0)
            {
                currentValue = 0;
            }
            else {
                currentValue = value;
            }
            

            currentFill = currentValue / MaxValue;
            healthText.text = currentValue + "/" + MaxValue;
        }
    }
   
    //dohvaćanje slike
    void Start () {
        content = GetComponent<Image>();
	}
	//regulacija popunjenosti slike
	void Update () {
        if (currentFill != content.fillAmount) {

            
            content.fillAmount = currentFill;
        }
     }
    //inicijalizacija 
    public void init (float curValue,float maxValue)
    {
        MaxValue = maxValue;
        CurrentValue = curValue;
        
        
    }
}
